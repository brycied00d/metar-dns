#!/usr/bin/env ruby
# frozen_string_literal: true

require 'metar'
require 'rubydns'

DOMAIN = 'weather.local'

INTERFACES = [
  [:udp, '0.0.0.0', 5300],
  [:tcp, '0.0.0.0', 5300]
].freeze

RubyDNS.run_server(INTERFACES) do
  match(/raw.(\w{4})\.metar\.#{DOMAIN}/,
        Resolv::DNS::Resource::IN::TXT) do |transaction, icao|
    station = Metar::Station.find_by_cccc(icao[1].upcase)
    transaction.respond!(station.raw)
  end
  match(/(\w{4})\.metar\.#{DOMAIN}/,
        Resolv::DNS::Resource::IN::TXT) do |transaction, icao|
    station = Metar::Station.find_by_cccc(icao[1].upcase)
    transaction.respond!(station.report.to_s.encode("ASCII", undef: :replace))
  end
end
